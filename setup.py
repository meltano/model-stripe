from setuptools import setup, find_packages

setup(
    name="model-stripe",
    version="0.6",
    description="Meltano .m5o models for data fetched using the Stripe API",
    packages=find_packages(),
    package_data={"models": ["*.m5o", "**/*.m5o"]},
    install_requires=[],
)
